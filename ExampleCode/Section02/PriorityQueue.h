/****************************************
 * File name: PriorityQueue.h 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the class methods for a 
 * simple Priority Queue. This class uses the 
 * queue member class priority_queue and 
 * *************************************/

#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H

#include <queue> 

template <typename T>
class PriorityQueue{
    
    private:
        std::priority_queue<T> pQueueEx;
        
    public:

        /*********************************************
        * Function Name: PriorityQueue 
        * Preconditions: none  
        * Postconditions: none  
        * Empty Constructor
        * ******************************************/     
        PriorityQueue() {}

        /*********************************************
        * Function Name: emplace 
        * Preconditions: T&  
        * Postconditions: none  
        * Emplaces an item in the queue in priority order
        * ******************************************/           
        void emplace(T& item){
            pQueueEx.emplace(item);
        }

        /*********************************************
        * Function Name: size() 
        * Preconditions: none  
        * Postconditions: size_t  
        * Returns the number of queue elements
        * ******************************************/          
        size_t size(){
            return pQueueEx.size(); 
        }

        /*********************************************
        * Function Name: top() 
        * Preconditions: none  
        * Postconditions: const T&  
        * Returns the address to the top element in 
        * the priority queue 
        * ******************************************/           
        const T& top(){
            return pQueueEx.top(); 
        }

        /*********************************************
        * Function Name: pop() 
        * Preconditions: none  
        * Postconditions: none
        * Removes the top element from 
        * the priority queue 
        * ******************************************/         
        void pop()
        {
            pQueueEx.pop();
        }
};

#endif
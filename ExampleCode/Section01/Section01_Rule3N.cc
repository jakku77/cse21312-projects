/*************************************
 * Filename: Section1_Rule3N.cc 
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu
 * 
 * This file contains an IntVec class
 * that does not meet the "Rule of 3"
 * **********************************/

#include<iostream>

class IntVec {
public:

    /***********************
     * Function Name: IntVec
     * Preconditions: int
     * Postconditions: none
     * This is the IntVec constructor
     * *********************/
   IntVec(int n): data(new int[n]), size(n) { }
   
   /***********************
     * Function Name: ~IntVec
     * Preconditions: none
     * Postconditions: none
     * This is the IntVec destructor
     * *********************/
   ~IntVec() { 
       delete[] data; 
    }
   
   /***********************
     * Function Name: operator[]
     * Preconditions: int
     * Postconditions: int &
     * Returns the address of the nth value
     * of the array
     * *********************/
   int& operator[](int n){ 
       return data[n]; 
   }
   
   /***********************
     * Function Name: operator []
     * Preconditions: int
     * Postconditions: const int&
     * 
     * Returns a constant address to the nth
     * element of the data array
     * *********************/
   const int& operator[](int n) const{ 
       return data[n]; 
   }

private:
   int* data;
   int size;
};

/****************
 * Function Name: main
 * Preconditions: int, char**
 * Postconditions: int
 * This is the main driver Function
 * ***************/
int main(int argc, char **argv)
{
   IntVec x(100);
   IntVec y = x; 
   
   std::cout << "The " << 5 << "th value of IntVec x is " << x[5] << std::endl;
   std::cout << "The " << 5 << "th value of IntVec y is " << y[5] << std::endl;
  
   return 0;
}

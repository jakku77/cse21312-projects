/*************************************
 * File name: Section01_LL_Struct.cc 
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu 
 * 
 * This file is a test of the LinkedList template 
 * class, with two functions for removing removing 
 * duplicates. The node is a struct of the LL class 
 * ***********************************/

#include <iostream>
#include <unordered_map>
#include "Section01_LinkedList_NodeStruct.h"

/****************************
* Function Name: removeDuplicates_1
* Preconditions: LinkedList<T>* theList
* Postconditions: none 
* This function takes a pointer to a linked list 
* and removes all duplicates in the list.
* First implementation uses undordered_map run time is O(n) with O(n) space
*****************************/
template<class T>
void removeDuplicates_1(LinkedList<T>* theList)
{
    if (theList->head == NULL)
    {
        return;
    }

    std::unordered_map<T,bool> map;
    Node<T>* current = theList->head;
    map[current->data] = 1;

    while (current->next)
    {
        if (map[current->next->data] == 0)
        {
            map[current->next->data] = 1;
            current = current->next;
        }
        else
        {
            //current->removeElementO_n_Space(current);
            theList->removeNode(current);
        }
    }
}

/****************************
* Function Name: removeDuplicates_1
* Preconditions: LinkedList<T>* theList
* Postconditions: none 
* This function takes a pointer to a linked list 
* and removes all duplicates in the list.
* This implementation uses no additional data structure run time is O(n^2) with O(1) space
*****************************/
template<class T>
void removeDuplicates_2(LinkedList<T>* theList)
{
    Node<T>* head = theList->head;
    
    if (theList->head == NULL)
    {
        return;
    }

    Node<T>* current = head;
    Node<T>* runner;

    while (current)
    {
        runner = current;
        
        while (runner->next)
        {
            if (current->data == runner->next->data)
            {
                theList->removeNode(runner);
            }
            else
            {
                runner = runner->next;
            }
        }

        current = current->next;
    }
}

/****************************
* Function Name: main 
* Preconditions: int, char** 
* Postconditions: int 
* This is the main driver function.
*****************************/
int main()
{
    LinkedList<int>* myList = new LinkedList<int>();
    myList->insert(5);
    myList->insert(7);
    myList->insert(12);
    myList->insert(7);
    myList->insert(16);
    myList->insert(16);
    myList->insert(25);
    myList->insert(16);
    myList->insert(25);
    myList->insert(11);
    myList->insert(19);

    std::cout << "The original list is: ";
    myList->display();
    std::cout << "The length of the LinkedList is " << myList->getLength() << std::endl;

    // Runs removeDuplicates_1 to run that implementation
    removeDuplicates_1(myList);
    std::cout << "The list with removeDuplicates_1 duplicated removed is: ";
    myList->display();   
    std::cout << "The length of the LinkedList is " << myList->getLength() << std::endl;
    
    myList->insert(19);
    myList->insert(25);
    myList->insert(11);
    myList->insert(5);    
    myList->insert(7);  
    
    std::cout << "The new original list is: ";
    myList->display();
    std::cout << "The length of the LinkedList is " << myList->getLength() << std::endl;
    
    // Runs removeDuplicates_2 to run that implementation
    removeDuplicates_2(myList);

    std::cout << "The new list with removeDuplicates_2 duplicated removed is: ";
    myList->display();
    std::cout << "The length of the LinkedList is " << myList->getLength() << std::endl;
    
    delete myList;
    
    return 0;
}